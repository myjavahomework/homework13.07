public class DogIsNotReadyException extends Throwable {
    public DogIsNotReadyException(String message) {
        super(message);
    }
}

